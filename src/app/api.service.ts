import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiService {
    constructor(public http: Http){}


    users = []

    getUsers() {
        this.http.get('http://backend-node-iot-ocp.apps02-london.amosdemo.io/users').subscribe(res =>{
        this.users = res.json()
        })

    }
    getProfile(id) {
        return this.http.get('http://backend-node-iot-ocp.apps02-london.amosdemo.io/profile/' +id)  
    }
   


    sendUserRegisteration(regData) {
        this.http.post('http://backend-node-iot-ocp.apps02-london.amosdemo.io/register', regData).subscribe(res =>{
        console.log(res.json());
	})

    }
   
    loginUser(loginData) {
        this.http.post('http://http://backend-node-iot-ocp.apps02-london.amosdemo.io/login', loginData).subscribe(res =>{
        console.log(res.json()._id);
        //console.log('thois ihgdhgdgfd' + res.ok);
        localStorage.setItem('id',res.json()._id)
        })

    }
   

    }
